def get_indices(arr, simb):
    ans = []

    for i in range(0, len(arr)):
        if (arr[i] == simb):
            ans.append(i)

    return ans        
